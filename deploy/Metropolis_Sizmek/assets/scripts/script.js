var timeUntilAutoCollapse = 10000; // Use 0 for no timeout
var autoCollapseTimeout;
var cancelAutoCollapseOnUserInteraction = true;
var lockScrollingWhenExpanded = true;
var isAndroid2 = (/android 2/i).test(navigator.userAgent);
var android2ResizeTimeout;

window.addEventListener("load", checkIfEBIsInitialized);
window.addEventListener("message", onMessageReceived);

function checkIfEBIsInitialized() {
    console.log('checkIfEBIsInitialized');
	if (EB.isInitialized()) {
		startAd();
	}
	else {
		EB.addEventListener(EBG.EventName.EB_INITIALIZED, startAd);
	}
}

function startAd() {
    console.log('startAd');
	initializeCustomVariables();
	addEventListeners();
	expand();
}

function initializeCustomVariables() {
    console.log('initializeCustomVariables');
	if (!EB._isLocalMode && EB._adConfig.customJSVars) {
		var customVariables = EB._adConfig.customJSVars;

		if (EBG.isNumber(customVariables.mdTimeUntilAutoCollapse)) {
			timeUntilAutoCollapse = customVariables.mdTimeUntilAutoCollapse;
		}

		if (EBG.isBool(customVariables.mdLockScrollingWhenExpanded)) {
			lockScrollingWhenExpanded = customVariables.mdLockScrollingWhenExpanded;
		}

		if (EBG.isBool(customVariables.mdCancelAutoCollapseOnUserInteraction)) {
			cancelAutoCollapseOnUserInteraction = customVariables.mdCancelAutoCollapseOnUserInteraction;
		}
	}
}


function addEventListeners() {
    console.log('addEventListeners');
    gwd.actions.events.addHandler('close_btn1','click',collapse,false);
    gwd.actions.events.addHandler('close_btn2','click',collapse,false);
	//document.getElementById("close_btn").addEventListener("click", collapse, false);
	//document.getElementById("user-action-button").addEventListener("click", userAction, false);
	//document.getElementById("gwd-taparea_1").addEventListener("click", clickthrough, false);

	if (cancelAutoCollapseOnUserInteraction) {
		//var ad = document.getElementById("page1");

        gwd.actions.events.addHandler('portrait-pg1','mousedown',cancelAutoCollapse,false);
        gwd.actions.events.addHandler('portrait-pg1','touchstart',cancelAutoCollapse,false);
        gwd.actions.events.addHandler('landscape-pg1','mousedown',cancelAutoCollapse,false);
        gwd.actions.events.addHandler('landscape-pg1','touchstart',cancelAutoCollapse,false);
	}
    console.log('addEventListeners2');
}

function userAction(event) {
	EB.userActionCounter("UserAction");
}

function clickthrough(event) {
	EB.clickthrough();
}

function cancelAutoCollapse(event) {
	clearTimeout(autoCollapseTimeout);
	   gwd.actions.events.removeHandler('portrait-pg1','mousedown',cancelAutoCollapse,false);
        gwd.actions.events.removeHandler('portrait-pg1','touchstart',cancelAutoCollapse,false);
        gwd.actions.events.removeHandler('landscape-pg1','mousedown',cancelAutoCollapse,false);
        gwd.actions.events.removeHandler('landscape-pg1','touchstart',cancelAutoCollapse,false);
}

function expand() {
    console.log('expand call');
    
	EB.expand({
		actionType: EBG.ActionType.AUTO
	});

	if (lockScrollingWhenExpanded) {
		preventPageScrolling();
	}
	
	if (timeUntilAutoCollapse > 0){
		autoCollapseTimeout = setTimeout(collapse, timeUntilAutoCollapse);
	}
}

function preventPageScrolling() {
	document.addEventListener("touchmove", stopScrolling);
}

function stopScrolling(event) {
	event.preventDefault();
}

function collapse(event) {
	EB.collapse();

	if (lockScrollingWhenExpanded) {
		allowPageScrolling();
	}
	
	removeAd();
}

function allowPageScrolling() {
	document.removeEventListener("touchmove", stopScrolling);
}

function removeAd() {
    gwd.actions.events.setInlineStyle('portrait-pg1',"display:none;");
    gwd.actions.events.setInlineStyle('landscape-pg1',"display:none;");
	//document.getElementById("page1").style.display = "none";

	var message = {
		adId: getAdID(),
		type: "removeAd"
	};

	window.parent.postMessage(JSON.stringify(message), "*");
}

function getAdID() {
	if (EB._isLocalMode) {
		return null;
	}
	else {
		return EB._adConfig.adId;
	}
}

function onMessageReceived(event) {
	try {
		var messageData = JSON.parse(event.data);

		if (messageData.adId && messageData.adId === getAdID()) {
			if (messageData.type && messageData.type === "resize") {
				if (isAndroid2) {
					forceResizeOnAndroid2();
				}
			}
		}
	}
	catch (error) {
		EBG.log.debug(error);
	}
}

function forceResizeOnAndroid2() {
	document.body.style.opacity = 0.99;
	clearTimeout(android2ResizeTimeout);
	android2ResizeTimeout = setTimeout(function() {
		document.body.style.opacity = 1;
		document.body.style.height = window.innerHeight;
		document.body.style.width = window.innerWidth;
	}, 200);
}